from playsound import playsound
import requests
import tkinter as tk
import cv2
import numpy as np
import time
import os


def lineNotifyMessage():
    headers = {"Authorization": "Bearer 0mVIPf0TWtYn8A0EJ8uL7fHZAcueYHkBrgCS8uJa1V9", "Content-Type" : "application/x-www-form-urlencoded"}
    payload = {'message': '有東西在動'}
    r = requests.post("https://notify-api.line.me/api/notify", headers = headers, params = payload)
    return r.status_code
def cam():
    videoCapture = cv2.VideoCapture(0) #讀取攝影機
#    videoCapture.set(cv2.CAP_PROP_FRAME_WIDTH, 800) #更改影片長寬 
#    videoCapture.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
    success, frame = videoCapture.read()
    while success:
        success, frame = videoCapture.read()
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)#转换颜色从BGR到RGBA
        current_image = Image.fromarray(cv2image)#将图像转换成Image对象
        imgtk = ImageTk.PhotoImage(image=current_image)
        Panel1.imgtk = imgtk
        Panel1.config(image=imgtk)
        root.after(1, video_loop)
        # cv2.imshow('input q to exit', frame)
        # if cv2.waitKey(1) & 0xFF == ord('q'):         # 若按下 q 鍵則離開迴圈
        #     break
    videoCapture.release()
    cv2.destroyAllWindows() #關閉視窗
def open_mp4():
    videoCapture = cv2.VideoCapture('1.mp4') #讀取影片
    success, frame = videoCapture.read()
    while success :
        cv2.imshow('mp4player', frame) #顯示
        success, frame = videoCapture.read() #如果下一偵有讀到就讀取下一偵
        if cv2.waitKey(24) & 0xFF == ord('q'):
            break
    videoCapture.release()
    cv2.destroyAllWindows()

def save_mp4():
    videoCapture = cv2.VideoCapture(0)
    fourcc = cv2.VideoWriter_fourcc(*"X264") #解碼器
    out = cv2.VideoWriter('output.avi', fourcc, 30, (640,480))#設定輸出影片的格式
    success, frame = videoCapture.read()
    while success:
        cv2.imshow('input q to exit',frame)        
        out.write(frame)# 寫入影格
        success, frame = videoCapture.read() 
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    videoCapture.release()  
    out.release()
    cv2.destroyAllWindows()


def move():
    videoCapture = cv2.VideoCapture(0)
    hasMotion = False
    # 輸出目錄
    outputFolder = "../output"
    # 自動建立目錄
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    # 設定影像尺寸
    width = int(videoCapture.get(3))
    height = int(videoCapture.get(4))
    fps = videoCapture.get(5)
    name = "%s/%s.avi" % (outputFolder, time.strftime("%m-%d-%Y_%H-%M-%S"))
    print(name)
    # 計算畫面面積
    area = width * height

    # 初始化平均影像
    success, frame = videoCapture.read()
    avg = cv2.blur(frame, (4, 4))
    # avg_float = np.float32(avg)

    fourcc = cv2.VideoWriter_fourcc(*"X264") #解碼器
    out = cv2.VideoWriter(name, fourcc, fps, (width,height))#設定輸出影片的格式

    while success:
            
        # 模糊處理
        blur = cv2.blur(frame, (4, 4))
        # 計算目前影格與平均影像的差異值
        diff = cv2.absdiff(avg, blur)
        # 將圖片轉為灰階
        gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
        # 篩選出變動程度大於門檻值的區域
        ret, thresh = cv2.threshold(gray, 25, 255, cv2.THRESH_BINARY)
        # 使用型態轉換函數去除雜訊
        kernel = np.ones((5, 5), np.uint8)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations=2)
        
        # 產生等高線
        cnts, cntImg = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        for c in cnts:
            # 忽略太小的區域
            if cv2.contourArea(c) < 2500:
                continue
            # 計算等高線的外框範圍
            (x, y, w, h) = cv2.boundingRect(c)
            # 畫出外框
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            out.write(frame)# 寫入影格
            hasMotion = True


        # 畫出等高線（除錯用）
        cv2.drawContours(frame, cnts, -1, (0, 0, 255), 2)
        
        # 儲存有變動的影像 發出警示聲 傳送訊息
        if hasMotion:
            cv2.imwrite("%s/output_%s.jpg" % (outputFolder, time.strftime("%m-%d-%Y_%H-%M-%S")), frame)






        cv2.imshow('input q to exit', frame)            
        success, frame = videoCapture.read()

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    videoCapture.release()  
    out.release()
    cv2.destroyAllWindows()

def move_msg():
    videoCapture = cv2.VideoCapture(0)
    hasMotion = False
    # 輸出目錄
    outputFolder = "../output"
    # 自動建立目錄
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    # 設定影像尺寸
    width = int(videoCapture.get(3))
    height = int(videoCapture.get(4))
    fps = videoCapture.get(5)
    name = "%s/%s.avi" % (outputFolder, time.strftime("%m-%d-%Y_%H-%M-%S"))
    print(name)
    # 計算畫面面積
    area = width * height

    # 初始化平均影像
    success, frame = videoCapture.read()
    avg = cv2.blur(frame, (4, 4))
    # avg_float = np.float32(avg)

    fourcc = cv2.VideoWriter_fourcc(*"X264") #解碼器
    out = cv2.VideoWriter(name, fourcc, fps, (width,height))#設定輸出影片的格式

    while success:
            
        # 模糊處理
        blur = cv2.blur(frame, (4, 4))
        # 計算目前影格與平均影像的差異值
        diff = cv2.absdiff(avg, blur)
        # 將圖片轉為灰階
        gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
        # 篩選出變動程度大於門檻值的區域
        ret, thresh = cv2.threshold(gray, 25, 255, cv2.THRESH_BINARY)
        # 使用型態轉換函數去除雜訊
        kernel = np.ones((5, 5), np.uint8)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations=2)
        
        # 產生等高線
        cnts, cntImg = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        for c in cnts:
            # 忽略太小的區域
            if cv2.contourArea(c) < 2500:
                continue
            # 計算等高線的外框範圍
            (x, y, w, h) = cv2.boundingRect(c)
            # 畫出外框
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            out.write(frame)# 寫入影格
            hasMotion = True


        # 畫出等高線（除錯用）
        cv2.drawContours(frame, cnts, -1, (0, 0, 255), 2)
        
        # 儲存有變動的影像 發出警示聲 傳送訊息
        if hasMotion:
            cv2.imwrite("%s/output_%s.jpg" % (outputFolder, time.strftime("%m-%d-%Y_%H-%M-%S")), frame)
            playsound('Beep_Short.mp3')
            lineNotifyMessage()





        cv2.imshow('input q to exit', frame)            
        success, frame = videoCapture.read()

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    videoCapture.release()  
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    pass